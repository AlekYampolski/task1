import { ISocials } from '../../models/socials.model';

export interface IProfileState {
        userId: number;
        city: string;
        languages: Array<string>;
        social: Array<ISocials>;
}

export const initialProfileState: IProfileState = {
        userId: null,
        city: '',
        languages: [],
        social: []
};

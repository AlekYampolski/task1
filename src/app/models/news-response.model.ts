import { INews } from './news.model';

export interface INewsResponse {
    status: string;
    data: Array<INews>;
}

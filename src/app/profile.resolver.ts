// import { Store } from '@ngrx/store';
// import { Injectable } from '@angular/core';
// import { Resolve } from '@angular/router';
// import { Observable } from 'rxjs';
// import { IProfileState } from './store/state/profile.state';

// import { IAppState } from './store/state/app.state';
// import { take } from 'rxjs/operators';
// // import { } from './store/state/'
// import { GetProfile } from './store/actions/profile.actions';

// @Injectable()
// export class ProfileResolver implements Resolve<IProfileState>{
//     constructor( private _store: Store<IAppState>) {}

//     resolve(): Observable<IProfileState> {
//         console.log('resolve');
//         this.initProfileData();

//         return this.waitForProfileDataToLoad();
//     }

//     waitForProfileDataToLoad(): Observable<IProfileState> {
//         return this._store.select('profileData');

//     }

//     initProfileData(): void {
//         this._store.pipe(take(1)).subscribe( store => {
//             console.log('here we are',store);
//             if (store.access.isLogged === 'ok') {
//                 this._store.dispatch(new GetProfile());
//             }
//         })
//     }
// }
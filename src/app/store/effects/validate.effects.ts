import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import {
    ValidateActions,
    GetValidate, GetValidateError,
    GetValidateSuccess, GetValidateSuccessWithError } from '../actions/validate.actions';
import { IValidation } from '../../models/validation.model';
import { switchMap, catchError } from 'rxjs/operators';

@Injectable()
export class ValidateEffects {
    @Effect()
    getValidate$ = this._actions$.pipe(
        ofType<GetValidate>(ValidateActions.GetValidate),
        switchMap((params) => {
             return this._httpService.checkValidation({email: params.payload.email, password: params.payload.password});
        }
             ),
        switchMap((validate: IValidation) => {
            if (validate.status === 'ok') {
                return of(new GetValidateSuccess(validate));
            }
            if (validate.status === 'err') {
                return of(new GetValidateSuccessWithError(validate));
            }
        }),
        catchError( (err) => {
            return of(new GetValidateError(err));
        })
    );

    constructor(
        private _httpService: HttpService,
        private _actions$: Actions
    ) {}
}

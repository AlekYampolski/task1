import { IProfileState, initialProfileState } from '../state/profile.state';
import { ProfileActions, ProfileActionsTypes } from '../actions/profile.actions';

export const profileReducer = (
    state = initialProfileState,
    action: ProfileActionsTypes
): IProfileState => {
    switch (action.type) {
        case ProfileActions.GetProfileSuccess: {
            return {
                ...state,
                userId: action.payload.data.userId,
                city: action.payload.data.city,
                social: action.payload.data.social,
                languages: action.payload.data.languages
            };
        }
        case ProfileActions.GetProfileError: {
            return {
                ...state,
                ...initialProfileState
            };
        }

        default: {
            return state;
        }
    }
};

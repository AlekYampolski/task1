import { initialValidationState, IValidationState } from '../state/validation.state';
import { ValidateActions, ValidateActionsTypes } from '../actions/validate.actions';

export const validateReducer = (
    state = initialValidationState,
    action: ValidateActionsTypes
): IValidationState => {
    switch (action.type) {
        case ValidateActions.GetValidateSuccess: {
            return {
                ...state,
                isLogged: true,
                userId: action.payload.data.id,
                message: ''
            };
        }

        case ValidateActions.GetValidateError: {
            return {
                ...state,
                message: 'Error with connection'
            };
        }

        case ValidateActions.GetValidateSuccessWithError: {
            return {
                ...state,
                message: action.payload.message
            };
        }

        case ValidateActions.GetValidateLogout: {
            return {
                ...state,
                ...initialValidationState
            };
        }

        default: {
            return state;
        }
    }
};

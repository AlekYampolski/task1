export interface ISocials {
    label: string;
    link: string;
}

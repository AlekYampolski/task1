import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState, StateSelects } from '../../store/state/app.state';
import { INewsState } from '../../store/state/news.state';
import { takeUntil } from 'rxjs/operators';
import { INews } from 'src/app/models/news.model';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnDestroy {
  numberOfNews: number;
  listOfNews: INews[];

  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private _store: Store<IAppState>
  ) {
    this._store.select(StateSelects.listOfNews)
      .pipe(
        takeUntil(this._destroy$)
       )
      .subscribe((listOfNews: INewsState) => {
        this.numberOfNews = listOfNews.news.length;
        this.listOfNews = listOfNews.news;
      });
  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

}

import { Action } from '@ngrx/store';
import { IProfile } from '../../models/profile.model';

export enum ProfileActions {
    GetProfile = '[API Calls Profile] Get Profile Data',
    GetProfileSuccess = '[API Calls Profile] Get Profile Data Loaded Success',
    GetProfileError = '[API Calls Profile] Get Profile Data Loaded Error'
}

export class GetProfile implements Action {
    readonly type = ProfileActions.GetProfile;

    constructor(public payload: number) {}
}

export class GetProfileSuccess implements Action {
    readonly type = ProfileActions.GetProfileSuccess;

    constructor(public payload: IProfile ) {}
}

export class GetProfileError implements Action {
    readonly type = ProfileActions.GetProfileError;
}

export type ProfileActionsTypes = GetProfile | GetProfileSuccess | GetProfileError;

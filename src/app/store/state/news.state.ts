import { INews } from '../../models/news.model';

export interface INewsState {
    news: INews[];
}

export const initialNewsState: INewsState = {
    news: []
};

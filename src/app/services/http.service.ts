import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) { }

  private _APIVaidation = 'https://mysterious-reef-29460.herokuapp.com/api/v1/validate';
  private _APIProfile = 'https://mysterious-reef-29460.herokuapp.com/api/v1/user-info';
  private _APINews = 'https://mysterious-reef-29460.herokuapp.com/api/v1/news';

  public checkValidation({email, password}) {
    const headers = this._createHeaders();
    const body = {
      email,
      password
    };
    return this._http.post(this._APIVaidation, body , { headers } );

  }

  public getProfileInfo(userId: number) {
    return this._http.get(`${this._APIProfile}/${userId}`);
  }

  public getNews() {
    return this._http.get(this._APINews);
  }

  private _createHeaders() {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return headers;
  }
}

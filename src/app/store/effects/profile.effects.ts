import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { switchMap, catchError, tap } from 'rxjs/operators';

import { HttpService } from 'src/app/services/http.service';
import { GetProfile, GetProfileError, GetProfileSuccess, ProfileActions } from '../actions/profile.actions';
import { IProfile } from '../../models/profile.model';

@Injectable()
export class ProfileEffects {

    @Effect()
    getProfile$ = this._actions$.pipe(
        ofType<GetProfile>(ProfileActions.GetProfile),
        switchMap( (data: GetProfile) => {
            return this._httpService.getProfileInfo(data.payload);
        }),
        switchMap( (profile: IProfile) => {
            return of(new GetProfileSuccess(profile));
        }),
        catchError( () => of(new GetProfileError()))
    );

    constructor(
        private _httpService: HttpService,
        private _actions$: Actions,
        private _router: Router
    ) {}

}

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { IAppState, StateSelects } from '../store/state/app.state';
import { IValidationState } from '../store/state/validation.state';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    public _router: Router,
    private _store: Store<IAppState>,
    private _toastr: ToastrService
    ) {}

  getFromStore(): Observable<IValidationState> {
    return this._store.select(StateSelects.access);
  }

  canActivate(): Observable<boolean> {
    return this.getFromStore()
      .pipe(
        switchMap((validation: IValidationState) => {
          if (!validation.isLogged) {
            this._toastr.error('You need to login');
            this._router.navigate(['/login']);
          }
          return of(validation.isLogged);
        }),
        catchError( () => of(false))
      );
  }
}

import { ActionReducerMap } from '@ngrx/store';

import { newsReducer } from './news.reducers';
import { profileReducer } from './profile.reducers';
import { validateReducer } from './validate.reducers';
import { IAppState } from '../state/app.state';

export const appReducer: ActionReducerMap<IAppState, any> = {
    listOfNews: newsReducer,
    profileData: profileReducer,
    access: validateReducer,
};

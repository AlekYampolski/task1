import { Component, OnInit, OnDestroy } from '@angular/core';
import { IAppState } from '../../store/state/app.state';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ISocials } from 'src/app/models/socials.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  city: string;
  socials: ISocials[];
  languages: string[];

  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private _store: Store<IAppState>
  ) {}

  ngOnInit() {
    this._store.select('profileData')
      .pipe(
        takeUntil(this._destroy$)
      )
      .subscribe( profileData => {
        if (profileData) {
          this.city = profileData.city;
          this.socials = this.changeSocialOrder(profileData.social, 'web') ;
          this.languages = profileData.languages;
        }
      });
  }

  changeSocialOrder(socialsList: Array<ISocials>, fistLabel: string) {
    const firstItem = socialsList.find(news => news.label === fistLabel);
    const tempArr = socialsList.filter(news => news.label !== fistLabel);
    tempArr.unshift(firstItem);
    return tempArr;
  }


  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

}

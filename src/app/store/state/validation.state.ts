export interface IValidationState {
    isLogged: boolean;
    userId: number;
    message: string;
}

export const initialValidationState: IValidationState = {
    isLogged: false,
    userId: null,
    message: ''
};

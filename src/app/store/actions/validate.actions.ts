import { Action } from '@ngrx/store';
import { IValidation } from '../../models/validation.model';

export enum ValidateActions {
    GetValidate = '[API Calls Validate] Get Validate Data',
    GetValidateSuccess = '[API Calls Validate] Get Validate Data Successs',
    GetValidateSuccessWithError = '[API Calls Validate] Get Validate Data With Error',
    GetValidateError = '[API Calls Validate] Get Validate Data Error',
    GetValidateLogout = '[API Calls Validate] Log Out'
}

export class GetValidate implements Action {
    readonly type = ValidateActions.GetValidate;

    constructor(readonly payload: { email: string, password: string} ) {

    }
}

export class GetValidateSuccess implements Action {
    readonly type = ValidateActions.GetValidateSuccess;

    constructor(public payload: IValidation) {}
}

export class GetValidateSuccessWithError implements Action {
    readonly type = ValidateActions.GetValidateSuccessWithError;

    constructor(public payload: IValidation) {}

}

export class GetValidateError implements Action {
    readonly type = ValidateActions.GetValidateError;
    constructor(public payload: IValidation) {}
}

export class GetValidateLogout implements Action {
    readonly type = ValidateActions.GetValidateLogout;
}

export type ValidateActionsTypes = GetValidate | GetValidateSuccess | GetValidateError | GetValidateSuccessWithError | GetValidateLogout;

import { ISocials } from './socials.model';


export interface IProfileData {
    userId: number;
    city: string;
    social: Array<ISocials>;
    languages: Array<string>;
}

export interface IProfile {
    status: string;
    data: IProfileData;
}

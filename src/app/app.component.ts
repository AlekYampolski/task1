import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import { IAppState, StateSelects } from './store/state/app.state';
import { IValidationState } from './store/state/validation.state';
import { GetNews } from './store/actions/news.actions';
import { GetValidateLogout } from './store/actions/validate.actions';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public isLoggedIn: boolean;

  private _destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(
    private _store: Store<IAppState>,
    private toastr: ToastrService
    ) {
      this._store
        .select(StateSelects.access)
        .pipe(takeUntil(this._destroy$))
        .subscribe((accessOptions: IValidationState) => {
          this.isLoggedIn = accessOptions.isLogged;
        });

  }

  logout() {
    this._store.dispatch(new GetValidateLogout());
    this.toastr.success('You are logged out!');
  }

  ngOnInit() {
    this._store.dispatch(new GetNews());
  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }
}

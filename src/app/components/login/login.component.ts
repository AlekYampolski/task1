import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';

import { IAppState } from '../../store/state/app.state';
import { GetProfile, ProfileActionsTypes } from '../../store/actions/profile.actions';
import { ValidateEffects } from '../../store/effects/validate.effects';
import { GetValidate, ValidateActions, GetValidateSuccess,
  GetValidateSuccessWithError, GetValidateError } from '../../store/actions/validate.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProfileEffects } from 'src/app/store/effects/profile.effects';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  public isEmailValid: boolean;
  public isPasswordValid: boolean;
  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    public _router: Router,
    private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    private _validateEffects: ValidateEffects,
    private _profileEffects: ProfileEffects,
    private toastr: ToastrService
  ) { }

  get email(): AbstractControl { return this.loginForm.get('email'); }
  get password(): AbstractControl { return this.loginForm.get('password') as AbstractControl; }
  get formIsValid(): boolean { return this.loginForm.valid; }

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ]))
    });

    this._validateEffects.getValidate$
      .pipe(
        takeUntil(this._destroy$)
      )
      .subscribe((validation: GetValidateSuccess| GetValidateSuccessWithError | GetValidateError ) => {

        if (validation.type === ValidateActions.GetValidateSuccess) {
          const userId = validation.payload.data.id;
          this.validationSuccess(userId);
        }
        if (validation.type ===  ValidateActions.GetValidateError || validation.type ===  ValidateActions.GetValidateSuccessWithError ) {
          this.validationErrorFromBack(validation.payload.message);
        }
      });

    this._profileEffects.getProfile$
        .pipe(
          takeUntil(this._destroy$)
        ).subscribe( () => {
          this._router.navigate(['/profile']);
        });

  }

  onSubmit() {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    this._store.dispatch(new GetValidate({email, password}));
  }

  private validationSuccess(userId: number): void {
    this.toastr.success('You are logged in!');
    this._store.dispatch(new GetProfile(userId));

  }

  private validationErrorFromBack(error): void {
    this.loginForm.patchValue({
      password: ''
    });
    this.toastr.error(error);
  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

}

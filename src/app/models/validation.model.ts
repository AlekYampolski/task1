export interface IValidationData {
    id: number;
}


export interface IValidation {
    status: string;
    data: IValidationData;
    message: string;
}

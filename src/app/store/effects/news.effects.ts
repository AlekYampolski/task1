import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { switchMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

import { HttpService } from '../../services/http.service';
import { INewsResponse } from '../../models/news-response.model';
import { NewsActions, GetNews, GetNewsError, GetNewsSuccess} from '../actions/news.actions';
import { INews } from 'src/app/models/news.model';

@Injectable()
export class NewsEffects {
    @Effect()
    getNews$ = this._actions$.pipe(
        ofType<GetNews>(NewsActions.GetNews),
        switchMap(() => this._httpService.getNews()),
        map((news: INewsResponse) => {
            return news.data;
        }),
        switchMap( (news: INews[]) => {
            return of(new GetNewsSuccess(news));
        }),
        catchError(() => of(new GetNewsError()) )
    );

    constructor(
        private _actions$: Actions,
        private _httpService: HttpService
        ) {}
}

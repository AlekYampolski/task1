import { INewsState, initialNewsState } from './news.state';
import { IProfileState, initialProfileState } from './profile.state';
import { IValidationState, initialValidationState} from './validation.state';

export interface IAppState {
    listOfNews: INewsState;
    profileData: IProfileState;
    access: IValidationState;
}

export enum StateSelects {
    listOfNews = 'listOfNews',
    profileData = 'profileData',
    access = 'access',
}

export const initialAppState: IAppState = {
    listOfNews: initialNewsState,
    profileData: initialProfileState,
    access: initialValidationState
};

export function getInitialAppState(): IAppState {
    return initialAppState;
}

import { NewsActions, NewsAcionsTypes } from '../actions/news.actions';
import { initialNewsState, INewsState } from '../state/news.state';

export const newsReducer = (
    state = initialNewsState,
    action: NewsAcionsTypes
): INewsState => {
    switch (action.type) {
        case NewsActions.GetNewsSuccess: {
          return {
              ...state,
              news: action.payload
          };
        }

        default: {
            return state;
        }
    }
};

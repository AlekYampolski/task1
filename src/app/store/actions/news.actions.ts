import { Action } from '@ngrx/store';
import { INews } from '../../models/news.model';

export enum NewsActions {
    GetNews = '[API Calls News] Get News',
    GetNewsSuccess = '[API Calls News] Get News Loaded Success',
    GetNewsError = '[API Calls News] Get News Loaded Error'
}

export class GetNews implements Action {
    readonly type = NewsActions.GetNews;
}

export class GetNewsSuccess implements Action {
    readonly type = NewsActions.GetNewsSuccess;
    constructor(public payload: INews[]) {}
}

export class GetNewsError implements Action {
    readonly type = NewsActions.GetNewsError;
}

export type NewsAcionsTypes = GetNews | GetNewsSuccess | GetNewsError;
